package Test;

public class Point {
	
	    //les attributs de la classe Point
		private int x;
		private int y;
	    //Le constructeur d'initialisation
		public Point(int x, int y) {
			this.x = x;
			this.y = y;
		}
	    //Le constructeur sans param�tre 
	    public Point() {
	    }
	    // Les accesseurs = Les getters et setters
	public int getX() {
		return x;
	}
 
	public void setX(int x) {
		this.x = x;
	}
 
	public int getY() {
		return y;
	}
 
	public void setY(int y) {
		this.y = y;
	}
    // La m�thode distance
	public double distance() {
		return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));
	}
    // La m�thode toString
	public String toString() {
		return "La distance entre l�origine et le Point (" + x + "," + y
				+ ") est : " + this.distance();
	}
 
}

